using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class playerMovement : MonoBehaviour {

    public playerMove controller;
	public Animator animator;

    public Collider2D m_FootDisableCollider;
    public float health = 1;
    public Slider HealthBar;

    public float runSpeed = 40f;

	float horizontalMove = 0f;
	bool jump = false;
	bool crouch = false;

    bool hurt = false;

	// Update is called once per frame
	void Update () {

		horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

		animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

		if (Input.GetButtonDown("Jump"))
		{
			jump = true;
			animator.SetBool("IsJumping", true);
        }

		if (Input.GetButtonDown("Crouch"))
		{
			crouch = true;
		} else if (Input.GetButtonUp("Crouch"))
		{
			crouch = false;
		}

	}

    public void WantJumping()
    {
        jump = true;
        animator.SetBool("IsJumping", true);
    }

	public void OnLanding ()
	{
		animator.SetBool("IsJumping", false);
        if (m_FootDisableCollider != null)
            m_FootDisableCollider.enabled = true;
    }

	public void OnCrouching (bool isCrouching)
	{
		animator.SetBool("IsCrouching", isCrouching);
	}

	void FixedUpdate ()
	{
		// Move our character
		controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
		jump = false;
	}

    public void SetHealthBar(float he)
    {
        HealthBar.value = he;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Enemy")
        {
            health -= 0.1f;
            hurt = true;
            SetHealthBar(health);
            WantJumping();
            //animator.setBool("IsHurt", hurt);
        }
    }
}
