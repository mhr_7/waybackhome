﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLineIs : MonoBehaviour
{
    public Camera mainCam;
    public Transform startPos;

    public Sprite[] bgSprite;
    public GameObject[] tilemap, pools;
    public GameObject bgSpriteObj;

    private bool once = false;

    int itt = 0, ittGrid = 0;

    void changeLayout()
    {
        tilemap[ittGrid - 1].gameObject.SetActive(false);
        tilemap[ittGrid].gameObject.SetActive(true);
    }

    void changeBg()
    {
        bgSpriteObj.GetComponent<SpriteRenderer>().sprite = bgSprite[itt];
    }


    void RePool()
    {
        for (int i = 0; i < pools.Length; i++)
            pools[i].gameObject.GetComponent<EaglePool>().ReCreate();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            mainCam.transform.position = startPos.position;
            col.gameObject.transform.position = startPos.position;
            RePool();
            //col.gameObject.GetComponent<playerMovement>().SetHealthBar(1f);
            //col.gameObject.GetComponent<playerMovement>().health = 1f;
            Debug.Log("Going Hit");
            itt++;
            if(itt % 2 == 0)
            {
                ittGrid++;
                changeLayout();
            }

            changeBg();
        }
    }
}
