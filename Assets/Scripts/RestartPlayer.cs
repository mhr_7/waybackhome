﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartPlayer : MonoBehaviour
{
    public Transform startPoint;
    // called when the cube hits the floor
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Player"){
          col.gameObject.transform.position = startPoint.position;
        }
        Debug.Log("OnCollisionEnter2D");
    }
}
