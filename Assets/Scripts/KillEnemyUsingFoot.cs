﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEnemyUsingFoot : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "EnemyH")
        {
            Debug.Log(collision.gameObject.tag);
            collision.gameObject.transform.parent.GetComponent<EnemyMove>().isKilled = true;
            transform.parent.gameObject.GetComponent<playerMovement>().WantJumping();
        }
        //Collider myCollider = collision.contacts[0].thisCollider;
        // Now do whatever you need with myCollider.
        // (If multiple colliders were involved in the collision, 
        // you can find them all by iterating through the contacts)
    }
}
