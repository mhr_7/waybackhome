﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    //[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;
    //private Vector3 m_Velocity = Vector3.zero;

    public float speed, range, m_pos;
    public bool isFacingRight, isKilled;
    public GameObject feedBack, mainSprite;
    //private Rigidbody2D m_Rigidbody2D;
    private Collider2D m_myCollider;

    // Start is called before the first frame update
    void Start()
    {
        m_pos = range;
        isKilled = false;
        m_myCollider = this.gameObject.GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Move the object upward in world space 1 unit/second.
        transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);

        if (isKilled)
        {
            nowKilled();
            isKilled = false;
        }

        if(m_pos < 0)
        {
            Flip();
        }
        m_pos--;
    }

    void Flip()
    {

        isFacingRight = !isFacingRight;
        speed *= -1;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        m_pos = range;
    }

    void nowKilled()
    {
        feedBack.gameObject.SetActive(true);
        mainSprite.gameObject.SetActive(false);
        m_myCollider.enabled = false;
        gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
        speed = 0;
        
        Invoke("KillMe", 1f);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        //Debug.Log(col.gameObject.tag);
        if (col.gameObject.name == "Foot")
        {
            feedBack.gameObject.SetActive(true);
            mainSprite.gameObject.SetActive(false);
            //invoke("KillMe", 2);
        }
    }

    void KillMe()
    {
        Destroy(this.gameObject);
    }
}
