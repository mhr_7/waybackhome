﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MiniEagle
{
    [SerializeField]
    public float speed, range;
    public Transform pos;
}

public class EaglePool : MonoBehaviour
{
    public GameObject enemy, temp;

    public List<MiniEagle> m_eagles = new List<MiniEagle>();

    //public Transform[] pos;
    //public float[] speed, range;

    // Start is called before the first frame update
    void Start()
    {
        ReCreate();
    }

    public void ReCreate()
    {
        for (int i = 0; i < m_eagles.Count; i++)
        {
            temp = Instantiate(enemy, m_eagles[i].pos.position, Quaternion.identity);
            temp.gameObject.GetComponent<EnemyMove>().speed = m_eagles[i].speed;
            temp.gameObject.GetComponent<EnemyMove>().range = m_eagles[i].range;
            //temp.gameObject.layer = 10;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
